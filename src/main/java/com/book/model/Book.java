package com.book.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKS")
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int bookId;

	@Column(name = "TITLE")
	private String bookTitile;

	@Column(name = "AUTHOR")
	private String author;

	@Column(name = "YEAR")
	private String yearPublish;

	public String getBookTitile() {
		return bookTitile;
	}

	public void setBookTitile(String bookTitile) {
		this.bookTitile = bookTitile;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYearPublish() {
		return yearPublish;
	}

	public void setYearPublish(String yearPublish) {
		this.yearPublish = yearPublish;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

}
