package com.book.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.book.model.Book;
import com.book.service.BookApiService;

@RestController
@RequestMapping("/books")
public class BookApiController {

	@Autowired
	BookApiService service;

	@GetMapping("/")
	public List<Book> getBooks() {
		return service.getAllBooks();
	}

	@PostMapping("/add/")
	public List<Book> insertBook(@RequestBody Book book) {
		service.insertBooks(book);
		return service.getAllBooks();
	}
}
