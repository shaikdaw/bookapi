package com.book.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.book.dao.BookDAO;
import com.book.model.Book;

@Service
public class BookApiService {

	@Autowired
	BookDAO bookdao;

	public List<Book> getAllBooks() {
		return bookdao.findAll();
	}

	public List<Book> insertBooks(Book book) {
		bookdao.save(book);
		return bookdao.findAll();
	}
}
